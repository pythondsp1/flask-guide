# app.py

from flask import Flask, render_template, request, redirect, url_for, session
from flask_wtf import Form 
from wtforms.fields import RadioField, SubmitField

app = Flask(__name__) # application 'app' is object of class 'Flask'
app.config['SECRET_KEY'] = 'yourKeyHere!' # to avoid CSRF attack 

subjects = ["English", "Dance", "Maths", "Geography"]
interests = ["Writing", "Dancing", "Logics", "Earth"]

class SelectChoiceForm(Form): # define elements of form e.g. RadioField
    # choices : (name for python, display on HTML)
    choice = RadioField('Select Yes/No', choices=[('yes','Yes'), ('no','No')])
    submit = SubmitField('Submit')

# decorator 'app.route' binds the 'url' with 'function', 
# i.e. url of 'home page (/)' will call function 'index' and
# 'return' value will be send back to the browser. 
@app.route('/')  # root : main page
def index():
    session['my_interest'] = 0
    # by default, 'render_template' looks inside the folder 'template'
    return render_template('index.html') 

@app.route('/subject/<int:id>')
def subject(id):
    return render_template('subject.html', sub=subjects[id])


@app.route('/interest', methods=['GET', 'POST'])
def interest():
    id = session['my_interest']
    form = SelectChoiceForm()
    if form.validate_on_submit():
    # if request.method == 'POST':
        if request.form['choice'] == 'no':
            session['my_interest'] = id+1
            return redirect(url_for('interest'))
        else:
            return redirect(url_for('subject', id=id))
    return render_template('interest.html', interest=interests[id], form=form)

if __name__ == '__main__':
    # '0.0.0.0' = 127.0.0.1 i.e. localhost
    # port = 5000 : we can modify it for localhost
    app.run(host='0.0.0.0', port=5000, debug=True) # local webserver : app.run()